import './App.css';
import Form from './Components/EventForm';
import AppBar from "./Components/Header"
import React from 'react';

function App() {
  return (
   <>
    <AppBar/>
    <Form/> 
   </>  
  );
}

export default App;