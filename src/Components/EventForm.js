/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable eqeqeq */
import React, { Component } from "react";
import styles from "./EventForm.css";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Switch from "@material-ui/core/Switch";
import { FormControlLabel } from "@material-ui/core";
import FormLabel from "@material-ui/core/FormLabel";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import HelpIcon from "@material-ui/icons/Help";

class Form
 extends Component {
  state = {
    EventOption: "",
    CategoriesOption: "",
    Zonal: ["Kolkata", "USA", "UK", "Asian Pacific"],
    title: "",
    summary: "",
    registration: "",
    onlineLink: "",
    isValidTitle: true,
    isValidSummary: true,
    isValidReg: true,
    isValidLink: true,
  };

  validate = () => {
    let eventError = "";
    let titleError = "";
    let categoriesError = "";
    let summaryError = "";
    let registrationError = "";
    let onlineLinkError = "";
    let startDateError = "";
    let startTimeError = "";
    let endDateError = "";
    let endTimeError = "";
    let timezoneError = "";
    if (this.state.event == "" || this.state.event == null) {
      eventError = "Event cannot be blank";
    }
    if (this.state.title == "" || this.state.title == null) {
      titleError = "Title cannot be blank";
      this.setState({ isValid: false });
    }
    if (this.state.categories == "" || this.state.categories == null) {
      categoriesError = "Categories cannot be blank";
    }
    if (this.state.summary == "" || this.state.summary == null) {
      summaryError = "Summary cannot be blank";
    }
    if (this.state.registration == "" || this.state.registration == null) {
      registrationError = "Registration cannot be blank";
    }
    if (this.state.onlineLink == "" || this.state.onlineLink == null) {
      onlineLinkError = "Online Link cannot be blank";
    }
    if (this.state.startDate == "" || this.state.startDate == null) {
      startDateError = "Start Date cannot be blank";
    }
    if (this.state.startTime == "" || this.state.startTime == null) {
      startTimeError = "Start Time cannot be blank";
    }
    if (this.state.endDate == "" || this.state.endDate == null) {
      endDateError = "End Date cannot be blank";
    }
    if (this.state.endTime == "" || this.state.endTime == null) {
      endTimeError = "End Time cannot be blank";
    }
    if (this.state.timezone == "" || this.state.timezone == null) {
      timezoneError = "Timezone cannot be blank";
    }

    if (
      eventError ||
      titleError ||
      categoriesError ||
      titleError ||
      summaryError ||
      registrationError ||
      onlineLinkError ||
      startDateError ||
      startTimeError ||
      endDateError ||
      endTimeError ||
      timezoneError
    ) {
      this.setState({
        eventError,
        categoriesError,
        summaryError,
        titleError,
        registrationError,
        onlineLinkError,
        startDateError,
        startTimeError,
        endDateError,
        endTimeError,
        timezoneError,
      });
      return false;
    }

    return true;
  };

  initialState = () => {
    let eventError = "";
    let titleError = "";
    let categoriesError = "";
    let summaryError = "";
    let registrationError = "";
    let onlineLinkError = "";
    let startDateError = "";
    let startTimeError = "";
    let endDateError = "";
    let endTimeError = "";
    let timezoneError = "";
    let event = "";
    let title = "";
    let summary = "";
    let registration = "";
    let onlineLink = "";
    let startDate = "";
    let startTime = "";
    let endDate = "";
    let endTime = "";
    let timezone = "";
    console.log("in intitialState");
    this.setState({
      event,
      title,
      summary,
      registration,
      onlineLink,
      startTime,
      startDate,
      endTime,
      endDate,
      timezone,
      eventError,
      titleError,
      categoriesError,
      summaryError,
      registrationError,
      onlineLinkError,
      startDateError,
      startTimeError,
      endDateError,
      endTimeError,
      timezoneError,
    });
  };

  handleSubmit = (event) => {
    if (
      this.state.title === "" ||
      this.state.summary === "" ||
      this.state.registration === "" ||
      this.state.onlineLink === ""
    ) {
      this.setState({
        isValidTitle: false,
        isValidLink: false,
        isValidSummary: false,
        isValidReg: false,
      });
    } else {
      this.setState({
        isValidTitle: true,
        isValidSummary: true,
        isValidLink: true,
        isValidReg: true,
      });
      alert("Data Valid");
    }
    event.preventDefault();
  };

  handleChangeTitle = (event) => {
    if (event.target.value === "") {
      this.setState({ isValidTitle: false });
    } else {
      this.setState({ isValidTitle: true });
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleChangeSummary = (event) => {
    if (event.target.value === "") {
      this.setState({ isValidSummary: false });
    } else {
      this.setState({ isValidSummary: true });
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleChangeRegistration = (event) => {
    if (event.target.value === "") {
      this.setState({ isValidReg: false });
    } else {
      this.setState({ isValidReg: true });
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleChangeOnlineLink = (event) => {
    if (event.target.value === "") {
      this.setState({ isValidLink: false });
    } else {
      this.setState({ isValidLink: true });
    }
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleChangeCategories = (event) => {
    this.setState({ CategoriesOption: event.target.value });
  };

  handleChangeEvent = (event) => {
    this.setState({ EventOption: event.target.value });
  };

  render() {
    console.log(this.state);
    return (
      <form onSubmit={this.handleSubmit}>
        <Container maxWidth="lg">
          <div className="row">
            <div className="col">
              <h4 className={styles.heading}>Create Event</h4>
            </div>
            <div className="col">
              <button type="submit" className="btn-sm m-10 btn1">
                Create
              </button>
              <button
                id={styles.btn2}
                type="reset"
                className={"btn btn-light btn-sm btn2"}
              >
                Cancel
              </button>
            </div>
          </div>
          <hr />
        </Container>
        <Container maxWidth="sm">
          <FormControl style={{ width: "100%" }}>
            <InputLabel id="demo-controlled-open-select-label">
              Add Event in*
            </InputLabel>
            <Select
              placeholder="Select..."
              labelId="demo-controlled-open-select-label"
              id={styles.title}
              name="event"
              onChange={this.handleChange}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={"o1"}>Profile:Mayur Upadhyay</MenuItem>
              <MenuItem value={"o2"}>Profile:Bhavesh Upadhyay</MenuItem>
            </Select>
          </FormControl>
          <TextField
            id="standard-basic"
            name="title"
            onChange={this.handleChangeTitle}
            style={styles}
            label="Title *"
            helperText="(0/250)"
            style={{ width: "100%", paddingTop: "2%" }}
          />
          {!this.state.isValidTitle && (
            <div style={{ fontSize: 12, color: "red" }}>
              <p>Title should not be empty</p>
            </div>
          )}
          <FormControl style={{ width: "100%" }}>
            <InputLabel id="demo-controlled-open-select-label">
              Categories*
            </InputLabel>
            <Select
              placeholder="Select..."
              labelId="demo-controlled-open-select-label"
              id={styles.title}
              name="categories"
              onChange={this.handleChange}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={"o1"}>Animal Rights</MenuItem>
              <MenuItem value={"o2"}>Arts & Music</MenuItem>
              <MenuItem value={"o3"}>Being Vegan</MenuItem>
              <MenuItem value={"o4"}>Branding & Marketing</MenuItem>
            </Select>
          </FormControl>
          <div style={{ fontSize: 12, color: "red" }}>
            {this.state.categoriesError}
          </div>
          <TextField
            id="standard-basic"
            name="summary"
            onChange={this.handleChangeSummary}
            style={styles}
            label="Short Summary*"
            helperText="(0/500)"
            style={{ width: "100%", paddingTop: "2%" }}
          />
          {!this.state.isValidSummary && (
            <div style={{ fontSize: 12, color: "red" }}>
              <p>summary should not be empty</p>
            </div>
          )}
          <FormLabel style={{ marginTop: "4%", color: "black" }}>
            Type : Public
          </FormLabel>
          <TextField
            id="standard-basic"
            name="registration"
            onChange={this.handleChangeRegistration}
            style={styles}
            label="Registration Site"
            helperText="(0/1024)"
            style={{ width: "100%", paddingTop: "2%" }}
          />
          {!this.state.isValidReg && (
            <div style={{ fontSize: 12, color: "red" }}>
              <p>Registration should not be empty</p>
            </div>
          )}
          <Grid container style={{ marginTop: "4%" }}>
            <Grid item xs>
              <FormLabel style={{ color: "black" }}>
                Is this a virtual event?
              </FormLabel>
            </Grid>
            <Grid item xs>
              <FormControlLabel
                control={
                  <Switch
                    name="virualEvent"
                    color="primary"
                    onChange={this.handleChange}
                  />
                }
              />
            </Grid>
            <Grid item xs></Grid>
          </Grid>
          <TextField
            id="standard-basic"
            name="onlineLink"
            onChange={this.handleChangeOnlineLink}
            style={styles}
            label="Online Link *"
            style={{ width: "100%", paddingTop: "4%" }}
          />
          {!this.state.isValidLink && (
            <div style={{ fontSize: 12, color: "red" }}>
              <p>online Link should not be empty</p>
            </div>
          )}
          <Grid container spacing={2}>
            <Grid item xs={8}>
              <InputLabel
                id="demo-mutiple-name-label"
                style={{ marginTop: "10%" }}
              >
                Select Timezone *
              </InputLabel>
              <Select
                labelId="demo-mutiple-name-label"
                id="demo-mutiple-name"
                onChange={this.handleChange}
                name="timezone"
                style={{ width: "100%" }}
              >
                {this.state.Zonal.map((name) => (
                  <MenuItem key={name} value={name}>
                    {name}
                  </MenuItem>
                ))}
              </Select>
              <div style={{ fontSize: 12, color: "red" }}>
                {this.state.timezoneError}
              </div>
            </Grid>
            <Grid item xs>
              <FormLabel
                className="font-weight-bold"
                style={{ marginTop: "38%", color: "black" }}
              >
                GMT +5:30 <HelpIcon />
              </FormLabel>
            </Grid>
          </Grid>
          <Grid container spacing={3} style={{ marginTop: "2%" }}>
            <Grid item xs>
              <TextField
                id="startDate"
                label="Start Date*"
                type="date"
                name="startDate"
                defaultValue="2020-10-21"
                onChange={this.handleChange}
              />
              <div style={{ fontSize: 12, color: "red" }}>
                {this.state.startDateError}
              </div>
            </Grid>

            <Grid item xs>
              <TextField
                id="startTime"
                label="Start Time*"
                type="time"
                name="startTime"
                defaultValue="09:00"
                style={{ width: "100%" }}
                onChange={this.handleChange}
              />
              <div style={{ fontSize: 12, color: "red" }}>
                {this.state.startTimeError}
              </div>
            </Grid>

            <Grid item xs></Grid>
          </Grid>
          <Grid
            container
            spacing={3}
            style={{ marginTop: "2%", marginBottom: "6%" }}
          >
            <Grid item xs>
              <TextField
                id="endDate"
                label="End Date*"
                type="date"
                name="endDate"
                defaultValue="2020-10-21"
                onChange={this.handleChange}
              />
              <div style={{ fontSize: 12, color: "red" }}>
                {this.state.endDateError}
              </div>
            </Grid>

            <Grid item xs>
              <TextField
                id="endTime"
                label="End Time*"
                type="time"
                name="endTime"
                defaultValue="17:00"
                style={{ width: "100%" }}
                onChange={this.handleChange}
              />
              <div style={{ fontSize: 12, color: "red" }}>
                {this.state.endTimeError}
              </div>
            </Grid>

            <Grid item xs></Grid>
          </Grid>
          <CKEditor
            id={styles.ckeditor}
            editor={ClassicEditor}
            data=""
            onReady={(editor) => {
              console.log("Editor is ready to use!", editor);
            }}
            onChange={(event, editor) => {
              const data = editor.getData();
              console.log({ event, editor, data });
            }}
            onBlur={(event, editor) => {
              console.log("Blur.", editor);
            }}
            onFocus={(event, editor) => {
              console.log("Focus.", editor);
            }}
          />
          <FormLabel style={{ marginTop: "4%" }}>Attachments</FormLabel>{" "}
          <HelpIcon />
          <div className="text-center">
            <FormLabel style={{ marginTop: "4%" }}>
              Use option on top right section on screen to save your changes
            </FormLabel>
          </div>
        </Container>
      </form>
    );
  }
}

export default Form
;
